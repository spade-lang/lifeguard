use std::collections::HashMap;
use std::num::ParseIntError;
use std::path::{Path, PathBuf};

use color_eyre::eyre;
use log::error;
use thiserror::Error;

use crate::config::{Config, Host};

pub mod config;
pub mod github;
pub mod gitlab;
pub mod cli;

pub type Line = usize;
pub type Project = (String, String);

#[derive(Debug, Clone, Copy)]
pub enum ThingStatus {
    Open,
    Closed,
    Invalid,
}

#[derive(Debug, Clone)]
pub struct Comment<Event> {
    pub file: PathBuf,
    pub line: Line,
    pub project: Project,
    pub event: Event,
    pub message: String,
}

pub enum ParsedComment {
    Github(Comment<github::Event>),
    Gitlab(Comment<gitlab::Event>),
}

#[derive(Debug, Clone)]
pub struct CommentsByHost {
    pub github_comments: Vec<Comment<github::Event>>,
    pub gitlab_comments: Vec<Comment<gitlab::Event>>,
}

impl CommentsByHost {
    pub fn filter_events(&mut self) -> eyre::Result<()> {
        self.github_comments = comments_to_report(
            self.github_comments.drain(..).collect(),
            github::filter_events,
        );
        self.gitlab_comments = comments_to_report(
            self.gitlab_comments.drain(..).collect(),
            gitlab::filter_events,
        );

        Ok(())
    }

    pub fn format(self) -> Vec<((PathBuf, Line), String)> {
        let mut comments = vec![];

        comments.append(&mut format_comments(&self.github_comments));
        comments.append(&mut format_comments(&self.gitlab_comments));

        comments.sort();
        comments
    }
}

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("Can't understand url: {0:?}")]
    UnknownUrl(url::Url),

    #[error("Unknown url host: {0:?}")]
    UnknownUrlHost(String),

    #[error("Unknown url part: {0:?}")]
    UnknownUrlPart(String),

    #[error("Unknown prefix: {0:?}")]
    UnknownPrefix(String),

    #[error("Can't parse int: {0:?}")]
    ParseIntError(#[from] ParseIntError),

    #[error("Invalid event: {0:?}")]
    InvalidEvent(String),

    #[error("Invalid project: {0:?}")]
    InvalidProject(String),
}

pub fn format_comments<Event: std::fmt::Display>(
    comments: &[Comment<Event>],
) -> Vec<((PathBuf, Line), String)> {
    comments
        .iter()
        .map(|comment| {
            (
                (comment.file.clone(), comment.line),
                format!(
                    "[{}:{}] {}/{}{}{}",
                    comment.file.display(),
                    comment.line,
                    comment.project.0,
                    comment.project.1,
                    comment.event,
                    if comment.message.is_empty() {
                        format!("")
                    } else {
                        format!(": {}", comment.message)
                    },
                ),
            )
        })
        .collect()
}

pub fn filter_and_trim_if_starts_with<'s>(s: &'s str, p: &str) -> Option<&'s str> {
    s.starts_with(p).then(|| s.trim_start_matches(p).trim())
}

pub fn find_comments<'s>(source: &'s str, comment_prefix: &str) -> Vec<(Line, &'s str)> {
    source
        .lines()
        .enumerate()
        .map(|(i, line)| (i, line.trim()))
        .filter_map(|(i, line)| Some((i, filter_and_trim_if_starts_with(line, comment_prefix)?)))
        .collect()
}

pub fn comments_to_report<Event, F>(comments: Vec<Comment<Event>>, filter: F) -> Vec<Comment<Event>>
where
    Event: Clone + std::hash::Hash + Eq,
    F: Fn(&Project, Vec<Event>) -> Vec<Event>,
{
    // NOTE: Collecting the iterator into the hashmap directly would overwrite
    // duplicate projects which would only store a single comment for each project.
    let mut per_project = HashMap::new();
    for comment in comments.into_iter() {
        per_project
            .entry(comment.project.clone())
            .or_insert_with(Vec::new)
            .push(comment);
    }

    let mut comments_to_report = vec![];

    for (project, comments) in per_project.into_iter() {
        let mut comments_per_event: HashMap<Event, Vec<Comment<Event>>> = HashMap::new();
        for comment in comments.into_iter() {
            comments_per_event
                .entry(comment.event.clone())
                .or_insert_with(Vec::new)
                .push(comment);
        }

        let events: Vec<Event> = comments_per_event.keys().cloned().collect();

        for event in filter(&project, events) {
            comments_to_report.extend(comments_per_event.remove(&event).unwrap());
        }
    }

    comments_to_report
}

fn read_line(
    comment: &str,
    file: PathBuf,
    line: usize,
    config: &Config,
) -> Result<ParsedComment, ParseError> {
    let (event, message) = comment.split_once(" ").unwrap_or((comment, ""));

    if let Ok(url) = url::Url::parse(event.trim_end_matches(":")) {
        let host = url.host_str().ok_or(ParseError::UnknownUrl(url.clone()))?;
        match host {
            "gitlab.com" => gitlab::parse_url_comment(&url, file, line, message.to_string())
                .map(ParsedComment::Gitlab),
            "github.com" => github::parse_url_comment(&url, file, line, message.to_string())
                .map(ParsedComment::Github),
            _ => Err(ParseError::UnknownUrlHost(host.to_string())),
        }
    } else {
        let repository = config
            .repositories
            .iter()
            .find(|repo| event.starts_with(&repo.name));

        match repository {
            None => Err(ParseError::UnknownPrefix(event.to_string())),
            Some(repo) => {
                let event = event.trim_start_matches(&repo.name);
                // FIXME: This should be caught while parsing the config but that requires
                //        splitting the Config that is parsed and the Config that is sent
                //        as argument.
                let (owner, name) = repo
                    .project
                    .split_once("/")
                    .ok_or_else(|| ParseError::InvalidProject(repo.project.clone()))?;
                match repo.host {
                    Host::Gitlab => Ok(ParsedComment::Gitlab(Comment {
                        file,
                        line,
                        project: (owner.to_string(), name.to_string()),
                        event: gitlab::Event::parse(event)
                            .ok_or(ParseError::InvalidEvent(event.to_string()))?,
                        message: message.to_string(),
                    })),
                    Host::Github => Ok(ParsedComment::Github(Comment {
                        file,
                        line,
                        project: (owner.to_string(), name.to_string()),
                        event: github::Event::parse(event)
                            .ok_or(ParseError::InvalidEvent(event.to_string()))?,
                        message: message.to_string(),
                    })),
                }
            }
        }
    }
}

pub fn read_file<P: AsRef<Path>>(
    path: P,
    config: &Config,
) -> eyre::Result<(Vec<ParsedComment>, Vec<ParseError>)> {
    let source = std::fs::read_to_string(&path)?;

    Ok(find_comments(&source, &config.comment_prefix)
        .into_iter()
        .map(|(line, comment)| (line, comment.trim_start_matches(&config.prefix)))
        .map(|(line, comment)| read_line(comment, path.as_ref().to_path_buf(), line, config))
        .fold((vec![], vec![]), |(mut parsed, mut errors), result| {
            match result {
                Ok(p) => parsed.push(p),
                Err(e) => errors.push(e),
            }
            (parsed, errors)
        }))
}

pub fn read_files<I>(
    paths: I,
    config: &Config,
) -> eyre::Result<(Vec<ParsedComment>, Vec<ParseError>)>
where
    I: IntoIterator,
    I::Item: AsRef<Path>,
{
    let mut parsed_comments = vec![];
    let mut error_comments = vec![];

    for path in paths {
        let (mut new_parsed_comments, mut new_error_comments) = read_file(path, &config)?;
        parsed_comments.append(&mut new_parsed_comments);
        error_comments.append(&mut new_error_comments);
    }

    Ok((parsed_comments, error_comments))
}

pub fn split_by_host(comments: Vec<ParsedComment>) -> CommentsByHost {
    let (github_comments, gitlab_comments) =
        comments
            .into_iter()
            .fold((vec![], vec![]), |(mut github, mut gitlab), new| {
                match new {
                    ParsedComment::Github(comment) => github.push(comment),
                    ParsedComment::Gitlab(comment) => gitlab.push(comment),
                }
                (github, gitlab)
            });

    CommentsByHost {
        github_comments,
        gitlab_comments,
    }
}
