use graphql_client::{reqwest::post_graphql_blocking as post_graphql, GraphQLQuery};
use reqwest::blocking::Client;
use std::path::PathBuf;

use crate::{Comment, Line, ParseError, ThingStatus};

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/github-schema.graphql",
    query_path = "graphql/github-query.graphql",
    response_derives = "Debug"
)]
struct RepoView;

type IssueOrPullRequest = repo_view::RepoViewRepositoryIssueOrPullRequest;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Event {
    ThingClosed(u64),
}

impl Event {
    pub fn parse(s: &str) -> Option<Self> {
        s.chars().next().and_then(|c| match c {
            '#' => Some(Event::ThingClosed(
                s.split_once('#')
                    .unwrap()
                    .1
                    .trim_end_matches(":")
                    .parse()
                    .ok()?,
            )),
            _ => None,
        })
    }
}

impl std::fmt::Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Event::ThingClosed(id) => write!(f, "#{id}"),
        }
    }
}

fn closed(issue_or_pr: &IssueOrPullRequest) -> ThingStatus {
    // NOTE: This could be merged in the query with `on Closable` but we might
    // in the future want to now if it is a pull request or issue, not just
    // if it is closed or not.
    use repo_view::RepoViewRepositoryIssueOrPullRequest::{Issue, PullRequest};

    match issue_or_pr {
        Issue(issue) if issue.closed => ThingStatus::Closed,
        PullRequest(pr) if pr.closed => ThingStatus::Closed,
        _ => ThingStatus::Open,
    }
}

fn status_of_ids(owner: &str, name: &str, events: Vec<Event>) -> Vec<(Event, ThingStatus)> {
    let token = match std::env::var("LIFEGUARD_GITHUB_TOKEN") {
        Ok(token) => token,
        Err(e) => panic!("specify a github personal access token via env-variable LIFEGUARD_GITHUB_TOKEN (error: {e})"),
    };

    let client = Client::builder()
        .user_agent("graphql-rust/0.10.0")
        .default_headers(
            std::iter::once((
                reqwest::header::AUTHORIZATION,
                reqwest::header::HeaderValue::from_str(&format!("Bearer {}", token)).unwrap(),
            ))
            .collect(),
        )
        .build()
        .unwrap();

    events
        .into_iter()
        .map(|event| match event {
            Event::ThingClosed(id) => {
                let variables = repo_view::Variables {
                    owner: owner.to_string(),
                    name: name.to_string(),
                    number: id as i64,
                };
                let response_body = post_graphql::<RepoView, _>(
                    &client,
                    "https://api.github.com/graphql",
                    variables,
                )
                .unwrap();
                let response_data: repo_view::ResponseData =
                    response_body.data.expect("missing response data");
                (
                    event,
                    response_data
                        .repository
                        .as_ref()
                        .and_then(|repo| {
                            repo.issue_or_pull_request
                                .as_ref()
                                .map(|thing| closed(thing))
                        })
                        .unwrap_or(ThingStatus::Invalid),
                )
            }
        })
        .collect()
}

pub fn filter_events((owner, name): &(String, String), events: Vec<Event>) -> Vec<Event> {
    status_of_ids(owner, name, events)
        .into_iter()
        .filter_map(|(event, status)| matches!(status, ThingStatus::Closed).then(|| event))
        .collect()
}

pub fn parse_url_comment<'u>(
    url: &'u url::Url,
    file: PathBuf,
    line: Line,
    message: String,
) -> Result<Comment<Event>, ParseError> {
    let parse_url = |url: &'u url::Url| {
        let mut segments = url.path_segments()?;
        let owner = segments.next()?;
        let name = segments.next()?;
        let _event_kind = segments.next()?;
        let event_num = segments.next()?;
        Some((owner, name, event_num))
    };

    let (owner, name, event_num) = parse_url(url).ok_or(ParseError::UnknownUrl(url.clone()))?;

    let event_num = event_num.parse()?;

    let event = Event::ThingClosed(event_num);

    Ok(Comment {
        file,
        line,
        project: (owner.to_string(), name.to_string()),
        event,
        message: message.to_string(),
    })
}
