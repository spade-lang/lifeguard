use std::path::Path;

use color_eyre::eyre;
use log::info;

use crate::config::Config;
use crate::{read_files, split_by_host};

pub fn read_and_report<I>(
    paths: I,
    config: &Config,
    dry_run: bool,
) -> eyre::Result<()>
where
    I: IntoIterator,
    I::Item: AsRef<Path>,
{
    let (parsed_comments, error_comments) = read_files(paths, &config)?;

    if !error_comments.is_empty() {
        info!(
            "Failed to parse the following comments:\n  {:?}",
            error_comments
        );
    }

    let mut split_comments = split_by_host(parsed_comments);

    if !dry_run {
        split_comments.filter_events()?;
    }

    let comments = split_comments.format();

    for (_, comment) in comments {
        println!("{}", comment);
    }

    Ok(())
}
